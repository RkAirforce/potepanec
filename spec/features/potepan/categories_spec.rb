require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "T-Shirt", parent: taxonomy.root) }
  let!(:product) { create(:product, name: "RailsProduct", taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  context "カテゴリーページ移遷時" do
    it "サイドバーに作成したCategoriesのトグルがあるか" do
      expect(page).to have_selector("a", text: "Categories")
    end

    it "商品名/値段が表示されているか" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  context "カテゴリークリック時" do
    it "作成したT-Shirtのtaxonがあるか" do
      click_link "Categories"
      expect(page).to have_selector("a", text: "T-Shirt")
    end

    it "クリックしたカテゴリーのページに移遷するか" do
      click_link "Categories"
      click_link "T-Shirt"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  context "商品名をクリックした時" do
    it "商品詳細ページに移遷するか" do
      click_link "RailsProduct"
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
