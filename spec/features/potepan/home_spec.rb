require 'rails_helper'

RSpec.feature "Potepan::Homes", type: :feature do
  before do
    visit potepan_root_path
  end

  context "表示のテスト" do
    it "タイトルが正しく表示される" do
      expect(page).to have_title "BIGBAG Store"
    end
  end
end
