require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, name: "RelatedProduct", taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, name: "UnrelatedProduct", price: 500) }
  let!(:related_products_list) { create_list(:product, 3, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  context "商品詳細表示" do
    it "タイトルが正しく表示される" do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
    end

    it "商品の名前/説明/値段が正しく表示される" do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_content product.display_price
    end

    it "カテゴリーの一覧ページに戻れるか" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end
  end

  context "関連商品表示" do
    it "関連商品の名前/値段が正しく表示される" do
      within(".productsContent") do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end
    end

    it "関連のない商品の名前/値段は表示しない" do
      within(".productsContent") do
        expect(page).not_to have_content unrelated_product.name
        expect(page).not_to have_content unrelated_product.display_price
      end
    end

    it "関連商品の名前・値段が正しく表示される" do
      within(".productsContent") do
        click_link "RelatedProduct"
      end
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    it "作成した関連商品が最大個数(4つ)表示される" do
      within(".productsContent") do
        expect(all('.productBox').size).to eq 4
      end
    end
  end
end
