require 'rails_helper'

RSpec.describe "Potepan::ProductDecorator", type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, name: "UnrelatedProduct", price: 500) }
  let!(:related_products_list) { create_list(:product, 4, taxons: [taxon]) }

  context "取得している商品に含まれるタグ（taxons）から関連商品を取得する" do
    it "取得した商品を関連商品に含めない" do
      expect(product.related_products(4)).not_to include product
    end

    it "取得している商品の関連商品と作成した4つの商品の配列が一致する" do
      expect(product.related_products(4)).to match_array(related_products_list)
    end
  end
end
