require 'rails_helper'

RSpec.describe "Potepan::Home", type: :request do
  describe "GET /index" do
    it "トップページにアクセスできること" do
      get potepan_root_path
      expect(response).to have_http_status(:success)
    end
  end
end
