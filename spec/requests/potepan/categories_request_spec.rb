require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "カテゴリーページにアクセスできること" do
      expect(response).to have_http_status(:success)
    end

    it "カテゴリー名/商品の名前/値段が含まれていること" do
      expect(response.body).to include taxon.name
    end

    it "サイドバーにカテゴリーが含まれていること" do
      expect(response.body).to include taxonomy.name
    end

    it "カテゴリーページに商品名/価格が表示されること" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
    end
  end
end
