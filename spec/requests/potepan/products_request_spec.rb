require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, name: "RelatedProduct", taxons: [taxon]) }
    let!(:unrelated_product) { create(:product, name: "UnrelatedProduct", price: 500) }

    before do
      get potepan_product_path(product.id)
    end

    it "商品詳細ページにアクセスできること" do
      expect(response).to have_http_status(:success)
    end

    it "商品の名前/説明/値段が含まれていること" do
      expect(response.body).to include product.name
      expect(response.body).to include product.description
      expect(response.body).to include product.display_price.to_s
    end

    it "関連商品の名前/値段が含まれていること" do
      expect(response.body).to include related_product.name
      expect(response.body).to include related_product.display_price.to_s
    end

    it "関連のない商品の名前/値段は含まないこと" do
      expect(response.body).not_to include unrelated_product.name
      expect(response.body).not_to include unrelated_product.display_price.to_s
    end
  end
end
