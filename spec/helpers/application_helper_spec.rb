require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:product) { create(:product) }

  context "トップページの場合" do
    it "デフォルトのタイトルが表示されていること" do
      expect(page_title("")).to eq("BIGBAG Store")
    end
  end

  context "引数が渡されていない場合" do
    it "ArgumentErrorが発生するか" do
      expect { page_title }.to raise_error(ArgumentError)
    end
  end

  context "タイトルがnil値の場合" do
    it "デフォルトのタイトルが表示されていること" do
      expect(page_title(nil)).to eq("BIGBAG Store")
    end
  end

  context "商品詳細ページの場合" do
    it "商品名のタイトルが表示されていること" do
      expect(page_title(product.name)).to eq("#{product.name} - BIGBAG Store")
    end
  end
end
