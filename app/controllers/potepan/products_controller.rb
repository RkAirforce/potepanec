class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.friendly.find(params[:id])
    @related_products = @product.related_products(MAX_RELATED_PRODUCTS)
  end
end
