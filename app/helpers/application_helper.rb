module ApplicationHelper
  DEFAULT_TITLE = "BIGBAG Store".freeze

  def page_title(title)
    if title.blank?
      DEFAULT_TITLE
    else
      "#{title} - #{DEFAULT_TITLE}"
    end
  end
end
