module Potepan::ProductDecorator
  def related_products(amount)
    Spree::Product.in_taxons(taxons).
      where.not(id: id).distinct.
      includes(master: [:default_price, :images]).
      order(Arel.sql("RAND()")).
      limit(amount)
  end
  Spree::Product.prepend self
end
